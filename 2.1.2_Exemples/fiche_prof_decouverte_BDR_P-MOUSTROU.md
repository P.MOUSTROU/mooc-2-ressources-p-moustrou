**Thématique :** Base de données

**Notions liées :** Données en table de première

**Résumé de l’activité :** Activité débranchée en deux parties. Gestion de données inconvénients et avantages.

**Objectifs :** Découverte des bases de données relationnelles. Introduire le modèle relationnel sans trop de formalisation : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel. 

**Auteur :** Charles

**Durée de l’activité :** 2h 

**Forme de participation :** individuelle ou en binôme, en autonomie, collective 

**Matériel nécessaire :** Papier, stylo !
